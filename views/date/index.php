<!DOCTYPE html>
<html>
<head>
    <title>Probando fechas</title>
    <style type="text/css">
        .cuadro {
            background-color: #ccc;
            width: 80%;
            margin: 20px;
            padding: 10px;
        }
    </style>
</head>
<body>
    <h1>Zona horaria:</h1>
    <ul>
        <li>Podemos leer y definir la zona horaria:</li>
        <li>Esto también puede ser definido en el php.ini</li>
    </ul>

    <div class="cuadro">
    <?php
        echo "La zona horaria es " . date_default_timezone_get() . "<br>";

        echo '<hr>';
        echo strftime("Ahora parecen ser las %H:%M.", time());
        echo '<hr>';

        // echo strftime("Hoy es %A y son las %H:%M.", $date);

        date_default_timezone_set('Australia/Melbourne');

        echo "La cambiamos a Melburne: " . date_default_timezone_get() . "<br>";
        echo '<hr>';
        echo strftime("Ahora parecen ser las %H:%M.", time());
        echo '<hr>';
        date_default_timezone_set('Europe/Madrid');
        echo "Finalmente a " . date_default_timezone_get() . "<br>";
        echo '<hr>';
        echo strftime("Ahora parecen ser las %H:%M.", time()) . ", hora de" . date_default_timezone_get();
        echo '<hr>';
    ?>
    </div>
    <hr>

    <h1>Fecha unix</h1>
    <ul>
        <li>Las fechas internamente se manejan como fechas Unix, segundos transcurridos desde el 1/1/1970</li>

    </ul>

    <h2>Momento actual: función time()</h2>

    <div class="cuadro">
        <?php
            $timeUnix = time();
            echo "Ahora estamos en $timeUnix";
        ?>
    </div>

    <h2>Fechas a partir de texto, en inglés</h2>

    <div class="cuadro">
    <?php
    echo "Fecha actual Unix con parámetro 'now' -->" . strtotime("now") . "<br>";
    echo "Fecha pasada '15 May 2015' -->" . strtotime("15 May 2015") . "<br>";
    echo "Fecha actual + 1 hora -->" . strtotime("+1 hours") . "<br>";
    echo "Fecha actual + 1 día -->" . strtotime("+1 day") . "<br>";
    echo "Fecha actual + 1 semana -->" . strtotime("+1 week") . "<br>";
    echo "Fecha actual + 1 mes -->" . strtotime("+1 month") . "<br>";
    echo "Fecha actual + 1 año -->" . strtotime("+1 year") . "<br>";
    echo "Fecha actual + 1 año + 1 mes + 1 semana + 1 día + 1 hora -->" . strtotime("+1 year +1 month +1 week +1 day +1 hours") . "<br>";
    echo "Próximo lunes -->" . strtotime("next monday") . "<br>";
    echo "El pasado lunes -->" . strtotime("last monday") . "<br>";
    echo "Próxima semana -->" . strtotime("next week") . "<br>";
    echo "Próximo mes -->" . strtotime("next month") . "<br>";
    echo "Próximo año -->" . strtotime("next year") . "<br>";
    ?>

    </div>



    <h2>También podemos componer fechas: mktime</h2>

    <ul>
        <li>Función: mktime(hora, minutos, segundos, mes, dia, año);</li>
        <li>Todos los argumentos son opcionales</li>
        <li>Los argumentos, todos numéricos, se autoexplican</li>
        <li>En la versión 7 de php se ha eliminado la posibilidad de indicar el horario (verano/invierno)</li>
    </ul>

    <div class="cuadro">
        <?php
        $year70 = mktime(11, 0, 0, 1, 1, 1970);
        echo "El año 1970 a la 11 de la mañana es $year70";
        ?>
    </div>




    <h1>Mostrar y formatear fechas</h1>
    <h2>Función date()</h2>

    <ul>
        <li>La función date() es la más simple en php para este fin</li>
        <li>Formatea usando textos en inglés</li>
    </ul>


    <div class="cuadro">
        <?php
            $today = time();
            $tomorrow = strtotime('tomorrow');
        ?>
        <ul>
            <li><?php echo "Hoy es el día " . date('j'); ?></li>
            <?php  var_dump($tomorrow);?>
            <li><?php echo "Hoy es el día " . date('j', $today); ?></li>
            <li><?php echo "Y mañana el día " . date('j', $tomorrow); ?></li>
            <li><?php echo "Estamos en el mes " . date('m'); ?></li>
            <li><?php echo "O " . date('M') . ", o " .  date('F'); ?></li>
            <li>Más casos en <a href="http://php.net/manual/es/function.date.php">php.net</a></li>
        </ul>
    </div>

    <ul>
    <li>Aquí más ejemplos sobre el día de hoy:</li>

    </ul>
    <div class="cuadro">

    <?php
        echo "Día del mes, sin ceros iniciales, de 1 a 31: ";
        echo date("j") . '<br>';
        echo "Día de la semana en inglés, con 3 letras, de Mon a Sun: ";
        echo date("D") . '<br>';
        echo "Día de la semana en inglés, de Sunday a Saturday: ";
        echo date("l") . '<br>';
        echo "del día de la semana, desde 1 (lunes) hasta 7 (domingo): ";
        echo date("N") . '<br>';
        echo "Sufijo del día del mes con 2 caracteres --> st, nd, : rd o th";
        echo date("S") . '<br>';
        echo "Número entero que representa el día de la semana, de: 0 (dom) a 6 (sab)";
        echo date("w") . '<br>';
        echo "Día del año, de 0 a 365: ";
        echo date("z") . '<br>';

     ?>
    </div>


    <h2>Más potente y multiidioma es strftime()</h2>


    <ul>
        <li>Usa un sistema parecido a printf (php, java, c)</li>
        <li>Por defecto estará en inglés</li>
    </ul>
    <div class="cuadro">
        <?php
        $date = strtotime('now');
        echo strftime("Hoy es %A y son las %H:%M.", $date);
        echo strftime(" El año es %Y y el mes es %B");
        ?>
    </div>
    <ul>
        <li>Con setlocale() podemos usar otros idiomas</li>
        <li>Los marcadores de formato podemos verlos en <a href="http://php.net/manual/en/function.strftime.php"> php.net</a></li>
    </ul>
    <div class="cuadro">
        <?php
        // setlocale(LC_ALL,'es_ES');
        setlocale(LC_ALL,'es_ES.UTF-8');
        $date = strtotime('now');
        echo strftime("Hoy es %A y son las %H:%M.", $date);
        echo strftime(" El año es %Y y el mes es %B");
        ?>
    </div>


    <h1>Además podemos descomponer la fecha: getdate()</h1>
        <p>Ejemplo para el día de hoy, y hora actual:</p>
        <div class="cuadro">
        <?php
        $fechaactual = getdate();
        echo "<pre>";
        print_r($fechaactual);
        echo "</pre>";
        echo "Hoy es: $fechaactual[weekday], $fechaactual[mday] de $fechaactual[month] de $fechaactual[year]";
        ?>
        </div>
        <p>Ejemplo para mañana:</p>
        <div class="cuadro">
        <?php
        $fechaactual = getdate(strtotime('tomorrow'));
        echo "<pre>";
        print_r($fechaactual);
        echo "</pre>";
        echo "Mañana será: $fechaactual[weekday], $fechaactual[mday] de $fechaactual[month] de $fechaactual[year]";
        ?>
        </div>

        <h1>Clase DateTime</h1>
        <ul>
            <li>DateTime es una clase existente en php y que facilita toda esta labor.</li>
            <li>Dejamos su estudio para otro momento</li>
        </ul>

        <div class="cuadro">

        <?php
            $date = new DateTime('now');
            echo 'Estamos a ' . $date->format('m-F-Y') . '<br>';
            $date->add(new DateInterval('P10D'));
            echo 'En diez días estaremos a ' . $date->format('m-F-Y') . '<br>';
        ?>
        </div>

        <ul>
            <li>Especialmente interesante el método siguiente</li>
        </ul>

        <div class="cuadro">
            <?php
            $fecha = DateTime::createFromFormat('Y-m-d', '2017-5-30');
            echo $fecha->format('d/m/Y');
            var_dump($fecha);
            ?>
        </div>
</body>
</html>
