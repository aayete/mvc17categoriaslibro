-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2017 at 12:37 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvc17`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `name`, `surname`, `birthdate`) VALUES
(1, 'Javier', 'Marías', NULL),
(2, 'Arturo', 'Pérez Reverte', '1950-01-01 00:00:00'),
(3, 'Juan', 'García', '1970-02-11 00:00:00'),
(4, 'María', 'Dueñas', '1951-12-01 00:00:00'),
(6, 'Fulano', 'De tal y tal', '2000-06-01 00:00:00'),
(7, 'nombre', 'apellidos', NULL),
(8, 'nombre', 'apellidos', '1995-05-01 00:00:00'),
(9, 'nuevonombre', 'yApellido', '1995-08-02 00:00:00'),
(10, 'Nombre1', 'Apellido1', '1980-04-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `pages` int(11) DEFAULT NULL,
  `cathegory_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `pages`, `cathegory_id`) VALUES
(1, 'El Perfume', ' Patrick Süskind', 3, 1),
(2, 'Lazarillo de Tormes', 'Lazarillo de Tormes', 153, 1),
(3, 'Etica para Amador', 'Fernando Savater', 111, 3);

-- --------------------------------------------------------

--
-- Table structure for table `cathegories`
--

CREATE TABLE `cathegories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cathegories`
--

INSERT INTO `cathegories` (`id`, `name`) VALUES
(1, 'novela'),
(2, 'manual'),
(3, 'ensayo'),
(4, 'atlas');

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `OrderID` int(11) NOT NULL,
  `OrderNumber` int(11) NOT NULL,
  `PersonID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'client', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `surname` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`, `surname`) VALUES
(1, 'Pepe', 'pepe@gmail.com', '$2y$10$Wv8aZwvImD3a94Y.d4sBUe/.b3leae2brrdfKvV4JE1jkdxV6qFYa', NULL, NULL, NULL, 1, 'García López'),
(2, 'Juan', 'juan@gmail.com', '$2y$10$GA8gjCpJafuHRIRmlmEbHeBbHmzExNRyFOZtLHSXilRGul62wMrE.', 'wlTRyTzY6u6HJ5DesgtAJgx4scyyCYYtPCztKBR4WSGQJt9FeK3MY17iKXcR', NULL, '2017-03-03 04:34:44', 2, 'Sánchez Moreno'),
(3, 'Ana', 'ana@gmail.com', '$2y$10$84HWrIKUxOE5H41uObdNw.N/g1z.sEgFCp2fn4L9oy2DFcHx7Msh6', NULL, NULL, NULL, 2, 'Merino Higuera'),
(4, 'Yolanda', 'yolanda@gmail.com', '$2y$10$gElO4seR31xYDpdrD7pHX.bqvnFO8j1UdglqA0Iw8lvBtW.AGqB2q', NULL, NULL, NULL, 2, 'Rodrigo Sorribas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_index` (`cathegory_id`);

--
-- Indexes for table `cathegories`
--
ALTER TABLE `cathegories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`OrderID`),
  ADD KEY `PersonID` (`PersonID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cathegories`
--
ALTER TABLE `cathegories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `cat_fk` FOREIGN KEY (`cathegory_id`) REFERENCES `cathegories` (`id`);

--
-- Constraints for table `Orders`
--
ALTER TABLE `Orders`
  ADD CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`PersonID`) REFERENCES `cathegories` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
