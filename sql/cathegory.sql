CREATE TABLE cathegories(
    id integer auto_increment primary key,
    name VARCHAR(50)
);

INSERT INTO cathegories
VALUES
(1, 'novela'),
(2, 'manual'),
(3, 'ensayo'),
(4, 'atlas');

ALTER TABLE books
ADD cathegory_id integer;

ALTER TABLE books
ADD INDEX `cat_index` (cathegory_id);

ALTER TABLE books
ADD constraint `cat_fk` FOREIGN KEY (cathegory_id) REFERENCES cathegories(id);
